from django.db import IntegrityError
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from django.views.decorators.http import require_http_methods
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("show_project", pk=task.project.pk)
    else:
        form = CreateTaskForm()
    context = {"form": form}
    return render(request, "tasks/new.html", context)


@require_http_methods(["POST"])
def mark_task_completed(request, pk):
    is_completed = request.POST.get("is_completed")
    task = Task.objects.get(pk=pk)
    print(task)
    try:
        task.is_completed = is_completed
        task.save()
    except IntegrityError:
        pass
    return redirect("show_my_tasks")


@login_required
def tasks_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/list.html", context)
