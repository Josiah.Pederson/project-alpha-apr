from django.urls import path
from tasks.views import (
    create_task,
    tasks_list,
    mark_task_completed,
)

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", tasks_list, name="show_my_tasks"),
    path("<int:pk>/complete/", mark_task_completed, name="complete_task"),
]
